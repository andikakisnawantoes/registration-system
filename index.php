<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Registraton System</title>
	<!-- css -->
	<link rel="stylesheet" type="text/css" herf="css/style.css">
	<!-- Bootstrap -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
	 integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
	 <!-- Bootstrap Bundel with popper -->
	 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<!-- fonts Shippori Antique -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Shippori+Antique&display=swap" rel="stylesheet">
	<!-- favicon link -->
	<link rel = "icon" href = "img/user.png" type = "img/x-icon">
	<!-- ajax -->
	<script
	src="https://code.jquery.com/jquery-3.6.0.min.js"
	integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
	crossorigin="anonymous"></script>
</head>
<body>
	<!-- heading block -->
	<div class="countainer-fluid" id="headings">
		<h1><strong>User Registration System</strong></h1>
		<h3 style= "color: #1e1f1f;">Sample project 1</h3>
</div>
<!-- Navigation bar -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-image: linear-gradient(-225deg, #ebf7ff 0%, #f2f2f2, #f9fff0 100%);">
<div class="container-fluid">
	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navabarSupportedContent"
	aria-controls="#navabarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
	<spam class="navbar-toggler-icon"></spam>
</button>
<div class="collapse navbar-collapse" id="navabarSupportedContent">
	<ul class="navabr-nav me-auto mb-2 mb-lg-0">
		<li class="nav-item">
			<a class="nav-link active" aria-current="page" heref="index.php"><b>HOMPAGE</b></a>
</li>

<li class="nav item">
	<a class="nav-link" href="display_use.php">Registreed User</a>
</li>

<li class="nav item">
	<a class="nav-link" href="about.php">About</a>
</li>
</ul>
<li class="d-flex">
	<a href ="login.php"><button class="btn btn-outline-success me-2" type="submit">LogIn</button></a>
</li>
</div>
</div>
</nav>

<div id="main-para" style="padding-left: 7%; padding-right: 7%;">
<h2><b> Welcome!</b></h2>
<br>
<ul>
	<li>sign up</li>
	<li>or register as new user.</li>
</ul>
<br>
<!-- boxes -->
<div class="row">
	<div class="col-sm-3">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title"><b>Users</b></h5>
				<p class="card-text">view list of registered users</p>
				<a href="display_user.php" calss="btn btn-primary">Go</a>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div calss="card">
			<div calss="card-boddy">
				<h5 class="card-title"><b>Register</b></h5>
				<p class="card-text">Register as new user</p>
				<a href="signup.php" class="btn btn-primary">Take me there</a>
				</div>
			</div>
		</div>
	</div>
	<br><br><br><br>
</div>
<footer class="foot>">
	<div><p style="margin: 0px; padding: 5px;">Thanks for visiting!</p></div>
</footer>
</body>
</html>